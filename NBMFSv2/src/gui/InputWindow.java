package gui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import org.apache.commons.io.FileUtils;

import logic.Check;
import logic.Load;
import logic.MessageBuilder;
import javax.swing.SwingConstants;
import java.awt.Color;

public class InputWindow extends JFrame {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JTextField txtHeader;
    private JTextField txtSender;
    private JTextField txtSubject;

    /**
     * Create the frame.
     */
    public InputWindow() {
    	setForeground(Color.RED);
        setResizable(false);
        setTitle("Napier Bank Message Filtering Service");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 454, 529);
        contentPane = new JPanel();
        contentPane.setFont(new Font("Formata", Font.PLAIN, 13));
        contentPane.setForeground(Color.RED);
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel lblHeader = new JLabel("Welcome to Napier Bank");
        lblHeader.setHorizontalAlignment(SwingConstants.CENTER);
        lblHeader.setFont(new Font("Formata", Font.PLAIN, 20));
        lblHeader.setBounds(18, 17, 257, 62);
        contentPane.add(lblHeader);

        JLabel lblNewLabel = new JLabel("");
        lblNewLabel.setIcon(new ImageIcon("/Users/steven/Desktop/repos/NBMFS2/NBMFSv2/assets/bank_logo_sm.png"));
        lblNewLabel.setBounds(287, 17, 150, 70);
        contentPane.add(lblNewLabel);

        JLabel lblEnterMessageHeader = new JLabel("Sender");
        lblEnterMessageHeader.setBounds(18, 158, 117, 14);
        contentPane.add(lblEnterMessageHeader);

        JLabel lblHeader_1 = new JLabel("Header");
        lblHeader_1.setBounds(18, 112, 117, 14);
        contentPane.add(lblHeader_1);

        JLabel lblEnterMessageText = new JLabel("Message");
        lblEnterMessageText.setBounds(18, 249, 117, 14);
        contentPane.add(lblEnterMessageText);

        txtHeader = new JTextField();
        txtHeader.setBounds(143, 109, 294, 20);
        contentPane.add(txtHeader);
        txtHeader.setColumns(10);
        txtHeader.addFocusListener(new FocusListener() {

            @Override
            public void focusLost(FocusEvent e) {
                Check.header(txtHeader.getText());
            }

            @Override
            public void focusGained(FocusEvent e) {
                // TODO Auto-generated method stub

            }
        });

        txtSender = new JTextField();
        txtSender.setColumns(10);
        txtSender.setBounds(143, 155, 294, 20);
        contentPane.add(txtSender);

        JTextArea txtText = new JTextArea();
        txtText.setWrapStyleWord(true);
        txtText.setLineWrap(true);
        txtText.setBounds(143, 248, 294, 253);
        contentPane.add(txtText);
        txtText.addFocusListener(new FocusListener() {

            @Override
            public void focusLost(FocusEvent e) {
                Check.messageBody(txtText.getText());
            }

            @Override
            public void focusGained(FocusEvent e) {
                // TODO Auto-generated method stub

            }
        });

        JButton btnSend = new JButton("Send");
        btnSend.setSelected(true);
        btnSend.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                MessageBuilder mb = new MessageBuilder(txtHeader.getText(), txtSender.getText(), txtSubject.getText(), txtText.getText());
            }

        });
        btnSend.setBounds(10, 295, 110, 23);
        contentPane.add(btnSend);

        JButton btnClear = new JButton("Clear");
        btnClear.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                txtHeader.setText("");
                txtSender.setText("");
                txtSubject.setText("");
                txtText.setText("");
            }
        });
        btnClear.setBounds(10, 330, 110, 23);
        contentPane.add(btnClear);

        JButton btnLoadMessages = new JButton("Load Messages");
        btnLoadMessages.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                JFileChooser fc = new JFileChooser();
                fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                fc.setDialogTitle("Select message folder...");
                
                int returnValue = fc.showOpenDialog(null);
                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    File file = fc.getSelectedFile();
                    System.out.println(file.getName());
                    
                    try {
						Load.messagesFromFile(file.getName());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                }
            }
   
        });
        btnLoadMessages.setBounds(10, 365, 110, 23);
        contentPane.add(btnLoadMessages);

        JLabel lblSubject = new JLabel("Subject (email)");
        lblSubject.setBounds(18, 202, 117, 14);
        contentPane.add(lblSubject);

        txtSubject = new JTextField();
        txtSubject.setColumns(10);
        txtSubject.setBounds(143, 199, 294, 20);
        contentPane.add(txtSubject);
    }
}

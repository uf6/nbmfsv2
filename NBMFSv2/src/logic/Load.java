/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

import messages.Message;
/**
 *
 * @author steven
 */
public class Load {

    public static void messagesFromFile(String filePath) throws IOException {
        
       List<Path> result = new ArrayList<>();
       Path dir = FileSystems.getDefault().getPath(filePath);
       
       try (DirectoryStream<Path> stream =
    		   Files.newDirectoryStream(dir, "*.json")) {
    			   for (Path aFile : stream) {
    				   result.add(aFile);
    				   System.out.println(aFile);
    			   }
    		   } catch (DirectoryIteratorException die) {
    			   throw die.getCause();
    		   }
    		   
	   Gson gson = new Gson();
	   for (Path aFile : result) {
		   String pathName = aFile.toString();
		   try (Reader reader = new FileReader(pathName)) {
			   Message msg = new Message();
			   msg = (Message) gson.fromJson(reader, msg.getClass());
			   System.out.println(msg.toString());
		   } catch (IOException ioe) {
			   ioe.printStackTrace();
		   }
    }


        
    }
}

package logic;

import java.util.regex.Pattern;

import javax.swing.JOptionPane;

import messages.Email;
import messages.SMS;
import messages.Tweet;

public class Check {
	
    public static void messageBody(String theMessageBody) {
        if (theMessageBody.length() == 0) {
            JOptionPane.showMessageDialog(null, "Message body cannot be empty!", "Error!", JOptionPane.WARNING_MESSAGE);
        }
    }

    public static void header(String theHeader) {
        boolean isValid = Pattern.matches("[SETset][0-9]{9}", theHeader);
        if (!isValid) {
            JOptionPane.showMessageDialog(null, "Message header must be in format 'S|E|T+123456789'", "Error!", JOptionPane.WARNING_MESSAGE);
        }
    }

    public static boolean sms (SMS theSms) {
        boolean isValidSender = Pattern.matches("\\+(9[976]\\d|8[987530]\\d|6[987]\\d|5[90]\\d|42\\d|3[875]\\d|2[98654321]\\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\\d{1,14}$",
                theSms.getSender());
        if (!isValidSender) {
            JOptionPane.showMessageDialog(null, "Sender's phone number must begin with a '+' and a valid country code");
            return false;
        }

        if (theSms.getMessageBody().length() > SMS.MAX_MESSAGE_LENGTH) {
            JOptionPane.showMessageDialog(null, "Ensure message is less than 140 characters\n"
                    + "Your message is " + theSms.getMessageBody().length() + "characters");
            return false;
        }
        System.out.println("At end of Check.sms()");
        return true;
    }
    
    public static boolean tweet(Tweet theTweet) {
        boolean isValidSender = Pattern.matches("^@[a-zA-Z0-9_]{1,15}$",
                theTweet.getSender());
        if (!isValidSender) {
            JOptionPane.showMessageDialog(null, "Sender must start with '@' followed by maximum 15 characters");
            return false;
        }
    	
    	return true;
    }
    
    public static boolean email(Email theEmail) {
    	// check sender
    	boolean isValidSender = Pattern.matches("\\b[\\w.%-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b",
                theEmail.getSender());
        if (!isValidSender) {
            JOptionPane.showMessageDialog(null, "Sender must be a valid email address. Please check");
            return false;
        }
    	
    	// check subject length
        
        if (theEmail.getSubject().length() > Email.getMaxSubjectLength()) {
        	JOptionPane.showMessageDialog(null, "Subject too long, 20 characters or less");
        	return false;
        }
        
        if (theEmail.getSubject().length() == 0) {
        	JOptionPane.showMessageDialog(null, "Subject cannot be empty!");
        	return false;
        }
    	
    	// check message length
        if (theEmail.getMessageBody().length() > Email.getMaxMessageLength()) {
        	JOptionPane.showMessageDialog(null, "Message is too long. Please remove "
        + (theEmail.getMessageBody().length() - Email.getMaxMessageLength())
        + " characters");
        	return false;
        }
        
    	return true;
    }
}

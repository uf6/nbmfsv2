/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;


/**
 *
 * @author steven
 */
public class MessageParser {
    
    static TreeMap<String, String> textSpeak = new TreeMap<>();
    
	public static void addSIR(String message) {
		String[] input = message.split("\\n");
		
		String sortCode = input[0];
		String SIR = input[1];
		
		System.out.println(sortCode);
		System.out.println(SIR);
		
		Session.significantIncidentReports.put(sortCode, SIR);
	}
	
	public static void showSirs() {
		
		String title = "Significant Incident Reports";
		String output = "";
		for (Map.Entry<String, String> e : Session.significantIncidentReports.entrySet()) {
			output = e.getKey() + "\t\t" + e.getValue() + "\n";
			output += "-------------------------------------------\n";
		}
		
		JOptionPane.showMessageDialog(null, output, title, JOptionPane.WARNING_MESSAGE);
	}
    
    
    public static String urls(String message) {
    	
    	
    	String[] input = message.split("\\s");
    	StringBuilder sb = new StringBuilder(message);
    	
    	String replacementText = "<URL quarantined>";
    	
    	for (String word : input) {
    		//boolean isUrl = Pattern.matches(url, word);
    		if (word.startsWith("http") || word.startsWith("www")) {
    			// add url to quarantine list
    			Session.addToQuarantine(word);
    			System.out.println(word);
    			System.out.println(word.length());
    			
    			// replace with "<URL quarantined>"
    			int startPos = sb.indexOf(word);
    			int endPos = sb.indexOf(word) + word.length() + 1; 
    			sb.replace(startPos, endPos, replacementText);
    		}
    	}
    	return sb.toString();
    }
    
    
    

    public static String textSpeak(String message) {
        loadTextSpeak();

        StringBuilder sb = new StringBuilder(message);
        int maxLength = sb.length();
        
        System.out.println(sb.toString());
        
        for (String key : textSpeak.keySet()) {
            if (sb.indexOf(key) != -1) {
                int startPos = sb.indexOf(key) + key.length() + 1;
                String addition = "<" + textSpeak.get(key) + "> ";

                if (startPos > maxLength) {
                    sb.append(addition);
                } else {
                    sb.insert(startPos, addition);
                }
            }
            maxLength = sb.length();
        }
        //System.out.println(sb.toString());

        return sb.toString();
    }
    
    public static void handlesAndMentions(String message) {
    	
    	String[] allTheWords = message.split("\\s");
    	
    	String hashtag = "^#\\S+$";
    	String mention = "^@[a-zA-Z0-9_]{1,15}$";
    	
    	
    	for (String word : allTheWords) {
    		boolean isHashtag = Pattern.matches(hashtag, word);
    		if (isHashtag) Session.addHashtag(word);
    		
    		boolean isMention = Pattern.matches(mention, word);
    		if (isMention) Session.addMention(word);
    	}
    }
    

    public static void loadTextSpeak() {
        String mPathToTextSpeakAbbrevs = "textwords.csv";
        String temp;
        File f = new File(mPathToTextSpeakAbbrevs);
        try {
            Scanner s = new Scanner(f);
            while (s.hasNextLine()) {
                temp = s.nextLine();
                String[] pair = temp.split(",");
                textSpeak.put(pair[0], pair[1]);
            }
            s.close();

        } catch (FileNotFoundException fnfe) {
            fnfe.printStackTrace();
        }
        System.out.println(textSpeak.toString());
    }
}
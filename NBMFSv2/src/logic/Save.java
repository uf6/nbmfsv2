/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic;

import com.google.gson.Gson;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.JOptionPane;
import messages.Message;

/**
 *
 * @author steven
 */
public class Save {

    public static void messageAsJson(Message msg) {
    Gson gson = new Gson();
        
        Random rand = new Random();
        String json = gson.toJson(msg);
        System.out.println(json);
        String fileName = "messages/" + msg.getHeader() + "_" + rand.nextInt(1000) + ".json";
        
        //String fileName = JOptionPane.showInputDialog("Enter name of file to save messages to: ");

        try (FileWriter fw = new FileWriter(fileName)) {
            gson.toJson(msg, fw);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.JOptionPane;

import messages.Message;

/**
 *
 * @author steven
 */
public class Session {

public static ArrayList<Message> messagesCreatedThisSession = new ArrayList<>();
public static ArrayList<Message> messagesRetrievedFromDisk = new ArrayList<>();
public static Date sessionStart;
public static Date sessionEnd;


public static ArrayList<String> mentions = new ArrayList<>();
public static ArrayList<String> hashtags = new ArrayList<>();
public static ArrayList<String> quarantinedUrls = new ArrayList<>();
public static ArrayList<String> incidentCodes = new ArrayList<>();
public static HashMap<String,String> significantIncidentReports = new HashMap<>();

	public static void loadIncidentCodes() {
		
		String[] codes = {"THEFT", "STAFF_ATTACK", "ATM_THEFT", "RAID", "CUSTOMER_ATTACK",
				"STAFF_ABUSE", "BOMB_THREAT", "TERRORISM", "SUSPICIOUS_INCIDENT", "INTELLIGENCE", "CASH_LOSS"};
		
		for (String code : codes) {
			incidentCodes.add(code);
		}
	}

	public static void addToQuarantine(String aUrl) {
		quarantinedUrls.add(aUrl);
	}
	
	public static ArrayList<String> getQuarantinedUrls() {
		return quarantinedUrls;
	}


	public static void addMention(String aMention) {
		mentions.add(aMention);
	}

	public static void addHashtag(String aHashtag) {
		hashtags.add(aHashtag);
	}
	
	public static void getMentions() {
		TreeMap<String,Integer> mentionsTable = new TreeMap<>();
		
		for (String s : mentions) {
			int count = Collections.frequency(mentions, s);
			mentionsTable.put(s, count);
		}
		
		String output = "";
		int counter = 0;
		for (String key : mentionsTable.keySet()) {
			counter++;
			output += counter + ". " + key + " : " + mentionsTable.getOrDefault(key, null) + "\n";
		}
		JOptionPane.showMessageDialog(null, output, "Popular handles", JOptionPane.INFORMATION_MESSAGE);
		
		System.out.println(output);

	}
	
	public static void getHashtags() {
		TreeMap<String,Integer> hashtagsTable = new TreeMap<>();
		
		for (String s : hashtags) {
			int count = Collections.frequency(hashtags, s);
			hashtagsTable.put(s, count);
		}
		
		String output = "";
		int counter = 0;
		for (String key : hashtagsTable.keySet()) {
			counter++;
			output += counter + ". " + key + " : " + hashtagsTable.getOrDefault(key, null) + "\n";
		}
		JOptionPane.showMessageDialog(null, output, "Trending Hashtags", JOptionPane.INFORMATION_MESSAGE);
		System.out.println(output);
	}
	
	
    public static void addMessage(Message msg) {
        messagesCreatedThisSession.add(msg);
    }
    
    
    public static void loadMessage(Message msg) {
        messagesRetrievedFromDisk.add(msg);
    }
    
    public static ArrayList<Message> getMessagesCreatedThisSession() {
        return messagesCreatedThisSession;
    }
    
    public static ArrayList<Message> getMessagesRetrievedFromDisk() {
        return messagesRetrievedFromDisk;
    }
}

package logic;

import messages.Email;
import messages.EmailNormal;
import messages.EmailSIR;
import messages.SMS;
import messages.Tweet;

public class MessageBuilder {

    public MessageBuilder(String header, String sender, String subject, String messageBody) {
        String typeOfMessage = header.substring(0, 1);
        switch (typeOfMessage.toUpperCase()) {
            case "S":
                SMS sms = new SMS(header, sender, messageBody);
                System.out.println("In messageBuilder()");
                if (Check.sms(sms)) {
//                    System.out.println(sms.toString());
                    sms.setMessageBody(MessageParser.textSpeak(sms.getMessageBody()));
                    sms.toString();
                    Save.messageAsJson(sms);
                }
                break;
		case "E":
			Email email = new Email(header, sender, subject, messageBody);
			
			if (Check.email(email)) {
				if (email.getSubject().substring(0,3).equals("SIR")) {
					EmailSIR esir = new EmailSIR(header, sender, subject, messageBody);
					System.out.println(esir.toString());
					Session.loadIncidentCodes();
					MessageParser.addSIR(esir.getMessageBody());
					esir.setMessageBody(MessageParser.urls(esir.getMessageBody()));
					Save.messageAsJson(esir);
					MessageParser.showSirs();

				} else {
					EmailNormal enormal = new EmailNormal(header, sender, subject, messageBody);
//					System.out.println(enormal.toString());
					enormal.setMessageBody(MessageParser.urls(enormal.getMessageBody()));
					enormal.toString();
					System.out.println(Session.getQuarantinedUrls().toString());
					Save.messageAsJson(enormal);
				}
			}
			
			break;
			
		case "T":
			Tweet tweet = new Tweet(header, sender, messageBody);
			if (Check.tweet(tweet)) {
				System.out.println(tweet.toString());
				tweet.setMessageBody(MessageParser.textSpeak(tweet.getMessageBody()));
				MessageParser.handlesAndMentions(tweet.getMessageBody());
				Session.getHashtags();
				Session.getMentions();
				Save.messageAsJson(tweet);
			}
			break;
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exp;

import java.io.IOException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;



/**
 *
 * @author steven
 */
public class FileChooser {
	static List<Path> listJsonFiles(Path dir) throws IOException {
		List<Path> result = new ArrayList<>();
		try (DirectoryStream<Path> stream = 
				Files.newDirectoryStream(dir, "*.json")) {
			for (Path entry : stream) {
				result.add(entry);
			}
		} catch (DirectoryIteratorException die) {
			throw die.getCause();
		}
		
		return result;
	}
	
	public static void main(String[] args) {
		Path path = FileSystems.getDefault().getPath("messages");
		List<Path> jsonFiles = new ArrayList<>();
		try {
			jsonFiles = listJsonFiles(path);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
			for (Path p : jsonFiles) {
				System.out.println(p);
			}
		}
	}
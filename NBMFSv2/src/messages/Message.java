package messages;
import javax.swing.JOptionPane;

public class Message {
	
	protected String header;
	protected String sender;
	protected String subject;
	protected String messageBody;
	protected String messageType;
	public static int MAX_MESSAGE_LENGTH = 140;

	
	public Message(String header, String sender, String subject, String messageBody) {
		this.header = header;
		this.sender = sender;
		this.subject = subject;
		this.messageBody = messageBody;
	}
	
	public Message(String header, String sender, String messageBody) {
		this.header = header;
		this.sender = sender;
		this.subject = null;
		this.messageBody = messageBody;
	}
        
    public Message() {}
	
	public String toString() {
		String output = "Type: " + messageType + "\n";
		output += "Header: " + header + "\n";
		output += "Sender: " + sender + "\n";
		output += "Subject: " + subject + "\n";
		output += "Message: " + messageBody + "\n";
		JOptionPane.showMessageDialog(null, output);
		return output;
	}
	
	
	public String getHeader() {
		return header;
	}
	public void setHeader(String header) {
		this.header = header;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getMessageBody() {
		return messageBody;
	}
	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}
	public String getMessageType() {
		return messageType;
	}
	
}

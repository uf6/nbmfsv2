package messages;

public class Tweet extends Message {
	public Tweet(String header, String sender, String messageBody) {
		super(header, sender, messageBody);
		this.messageType = "Tweet";
		
	}
}

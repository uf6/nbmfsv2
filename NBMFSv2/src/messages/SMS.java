package messages;

public class SMS extends Message {
	
	
	
	public SMS(String header, String sender, String messageBody) {
		super(header, sender, messageBody);
		this.messageType = "SMS";
	}
	
}

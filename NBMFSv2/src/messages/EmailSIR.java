package messages;

public class EmailSIR extends Email {
	public EmailSIR(String header, String sender, String subject, String message) {
		super(header, sender, subject, message);
		this.messageType = "Special Incident Report";
	}
}
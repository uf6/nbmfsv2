package messages;

public class EmailNormal extends Email {
	public EmailNormal(String header, String sender, String subject, String message){
		super(header, sender, subject, message);
		this.messageType = "Normal Email";
	}
}

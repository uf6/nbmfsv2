package messages;

public class Email extends Message {
	
	protected static final int MAX_SUBJECT_LENGTH = 20;
	private static final int MAX_MESSAGE_LENGTH = 1028;
	
	public Email(String header, String sender, String subject, String message) {
		super(header, sender, subject, message);
	}

	public static int getMaxMessageLength() {
		return MAX_MESSAGE_LENGTH;
	}
	
	public static int getMaxSubjectLength() {
		return MAX_SUBJECT_LENGTH;
	}

}
